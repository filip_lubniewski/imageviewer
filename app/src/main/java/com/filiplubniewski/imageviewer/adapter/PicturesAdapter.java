package com.filiplubniewski.imageviewer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.filiplubniewski.imageviewer.R;
import com.filiplubniewski.imageviewer.model.Picture;
import com.filiplubniewski.imageviewer.ui.BigPicture;

import java.util.ArrayList;

/**
 * Created by macbookpro on 28.11.15.
 */
public class PicturesAdapter extends RecyclerView.Adapter<PicturesAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Picture> mPictures;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView iv_picture;
        public Context context;
        public ArrayList<Picture> array;

        public ViewHolder(View itemView, Context context, ArrayList<Picture> array) {
            super(itemView);

            this.context = context;
            this.array = array;

            iv_picture = (ImageView) itemView.findViewById(R.id.picture_image_view);
            iv_picture.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, BigPicture.class);
            intent.putExtra("URL", array.get(getLayoutPosition()).getUrl());
            context.startActivity(intent);
        }
    }

    public PicturesAdapter(ArrayList<Picture> pictures, Context context) {
        mPictures = pictures;
        mContext = context;
    }

    @Override
    public PicturesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View pictureView = inflater.inflate(R.layout.item_picture, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(pictureView, mContext, mPictures);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Picture picture = mPictures.get(i);

        String thumbnailUrl = picture.getThumbnailUrl();

        ImageView iv = viewHolder.iv_picture;

        Glide.with(mContext)
                .load(thumbnailUrl)
                .placeholder(android.R.drawable.progress_horizontal)
                .into(iv);
    }

    @Override
    public int getItemCount() {
        return mPictures.size();
    }
}
