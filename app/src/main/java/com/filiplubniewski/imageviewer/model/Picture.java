package com.filiplubniewski.imageviewer.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by macbookpro on 27.11.15.
 */
public class Picture {
    private int mId;
    private String mUrl;
    private String mThumbnailUrl;

    public static Picture parseJson(JSONObject jsonObject) {
        Picture picture = new Picture();

        try {
            picture.mId = jsonObject.getInt("id");
            picture.mThumbnailUrl = jsonObject.getString("thumbnailUrl");
            picture.mUrl = jsonObject.getString("url");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return picture;
    }

    public static ArrayList<Picture> parseJson(JSONArray jsonArray) {

        int jsonArrayLength = jsonArray.length();
        ArrayList<Picture> array = new ArrayList<>(jsonArrayLength);
        Picture picture;
        JSONObject jsonObject;

        for (int i = 0; i < jsonArrayLength; i++) {
            jsonObject = null;

            try {
                jsonObject = jsonArray.getJSONObject(i);
            } catch(JSONException e) {
                e.printStackTrace();
            }

            picture = Picture.parseJson(jsonObject);

            if(picture != null) {
                array.add(picture);
            }
        }

        return array;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getThumbnailUrl() {
        return mThumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        mThumbnailUrl = thumbnailUrl;
    }
}
