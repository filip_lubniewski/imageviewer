package com.filiplubniewski.imageviewer.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.widget.Toast;

import com.filiplubniewski.imageviewer.R;
import com.filiplubniewski.imageviewer.adapter.PicturesAdapter;
import com.filiplubniewski.imageviewer.model.Picture;
import com.filiplubniewski.imageviewer.ui.recyclerviewdecoration.GridSpacingDecoration;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import pl.appformation.smash.Smash;
import pl.appformation.smash.SmashQueue;
import pl.appformation.smash.SmashRequest;
import pl.appformation.smash.SmashResponse;
import pl.appformation.smash.errors.SmashError;
import pl.appformation.smash.requests.SmashStringRequest;

public class MainActivity extends Activity {

    private RecyclerView mPicturesRecyclerView;
    private ArrayList<Picture> mPictureArrayList;
    private PicturesAdapter mAdapter;

    public final String URL = "http://jsonplaceholder.typicode.com/albums/1/photos";
    public final int IMAGE_SIZE = 150; // in pixels
    private int mColumnCount;
    private float dpScreenWidth;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPicturesRecyclerView = (RecyclerView) findViewById(R.id.rvPictures);
        mPictureArrayList = new ArrayList<>();
        mAdapter = new PicturesAdapter(mPictureArrayList, this);

        mPicturesRecyclerView.setHasFixedSize(true);
        mPicturesRecyclerView.setAdapter(mAdapter);

        setGridLayoutManager();
        setSpacingBetweenImages();

        if(!isOnline()) {
            Toast.makeText(this, R.string.internet_off, Toast.LENGTH_LONG)
                    .show();
        }
        else {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.json_download));
            mProgressDialog.show();
        }

        SmashQueue mSmashQueue = Smash.buildSmashQueue(this);
        SmashStringRequest request = new SmashStringRequest(SmashRequest.Method.GET, URL,
                new SmashResponse.SuccessListener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            JSONArray array = new JSONArray(s);
                            ArrayList<Picture> Pictures = Picture.parseJson(array);
                            mPictureArrayList.addAll(Pictures);
                            mAdapter.notifyDataSetChanged();
                            mProgressDialog.hide();
                        } catch(JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new SmashResponse.FailedListener() {
            @Override
            public void onFailedResponse(SmashError smashError) {
                Toast.makeText(getApplicationContext(), R.string.failed_response_smash, Toast.LENGTH_LONG)
                        .show();
            }
        });

        mSmashQueue.add(request);
    }

    private void setSpacingBetweenImages() {
        int spacing = (int) (dpScreenWidth - mColumnCount * IMAGE_SIZE) / (mColumnCount + 1);
        mPicturesRecyclerView.addItemDecoration(new GridSpacingDecoration(spacing));
    }

    private void setGridLayoutManager() {
        dpScreenWidth = getScreenWidth();
        mColumnCount = (int) dpScreenWidth / IMAGE_SIZE;
        GridLayoutManager gridLayoutManager =
                new GridLayoutManager(this, mColumnCount, GridLayoutManager.VERTICAL, false);
        mPicturesRecyclerView.setLayoutManager(gridLayoutManager);
    }


    private float getScreenWidth() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density  = getResources().getDisplayMetrics().density;
        float dpWidth  = outMetrics.widthPixels / density;

        return dpWidth;
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnectedOrConnecting();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
